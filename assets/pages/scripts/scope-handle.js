var ScopeComponent = (function(){
  
  var DOMStrings = {
    triggerScope: '#triggerScopeSelect',
    modalScope: '#modalScope'
  }
  
  var selectScopeModalHandle = function () {
    $(DOMStrings.triggerScope).bind('click', function (e) {
      e.preventDefault();
      $(DOMStrings.modalScope).modal('show');
    });
  }
  
  var showHideScope = function () {
    $(".trigger-scope").bind("click", function () {
      $(this).parents("#modalScope").find(".scope-item").removeClass("active");
      $(this).parent().addClass("active");
    });
    
    $("#trigger-show-scope").bind("click", function (e) {
      e.preventDefault();
      if ($("#box-scope").hasClass("my-hidden")) {
        $("#box-scope").removeClass("my-hidden");
      }
    });
    
    $("#trigger-hide-scope").bind("click", function (e) {
      e.preventDefault();
      if (!$("#box-scope").hasClass("my-hidden")) {
        $("#box-scope").addClass("my-hidden");
      }
    });
  }

  var triggerPhItem = function () {
    $(".active-phitem").bind("change", function () {
      $("#wrap-ph").find(".ph-item").removeClass("active");
      $(this).parents(".ph-item").addClass("active");
    });
  }
  
  return {
    init: function () {
      selectScopeModalHandle();
      showHideScope();
      triggerPhItem();
    }
  }
})();

jQuery(document).ready(function() {    
   ScopeComponent.init();
});


